/* -*- mode: c++ -*- */

#include "Kaleidoscope.h"

// Support for storing the keymap in EEPROM
#include "Kaleidoscope-EEPROM-Settings.h"
#include "Kaleidoscope-EEPROM-Keymap.h"

// Support for communicating with the host via a simple Serial protocol
#include "Kaleidoscope-FocusSerial.h"

// Support for controlling the keyboard's LEDs
#include "Kaleidoscope-LEDControl.h"

// Support for the "Boot greeting" effect, which pulses the 'LED' button for 10s
// when the keyboard is connected to a computer (or that computer is powered on)
#include "Kaleidoscope-LEDEffect-BootGreeting.h"

// Support for Keyboardio's internal keyboard testing mode
#include "Kaleidoscope-HardwareTestMode.h"

// Support for host power management (suspend & wakeup)
#include "Kaleidoscope-HostPowerManagement.h"

// Support for magic combos (key chords that trigger an action)
#include "Kaleidoscope-MagicCombo.h"

// Support for USB quirks, like changing the key state report protocol
#include "Kaleidoscope-USB-Quirks.h"

// Imports for my customizations
#include "Kaleidoscope-OneShot.h"
#include "Kaleidoscope-Macros.h"


/* *INDENT-OFF* */
enum { PRIMARY, NAVIGATION, NUMPAD, PUNC, FUNCTION };
enum { NavToNumpad, NavToPunc };

KEYMAPS(
  [PRIMARY] = KEYMAP_STACKED
  (___, ___, Key_Semicolon,   Key_H, Key_KeypadLeftParen,   Key_KeypadRightParen,   Key_Minus,
   ___, Key_Quote, Key_O, Key_R, Key_N, Key_F, Key_Tab,
   ___, Key_Comma, Key_E, Key_I, Key_T, Key_D,
   ___, Key_Period, Key_A, Key_S, Key_L, Key_C, Key_Esc,
   OSM(LeftControl), Key_Backspace, OSM(LeftGui), OSM(LeftShift),
   ShiftToLayer(FUNCTION),

   Key_Equals,  Key_LeftBracket,   Key_RightBracket,   Key_X, Key_Backtick,   ___, ___,
   Key_Enter,  Key_K, Key_G, Key_B, Key_Z, ___, ___,
         Key_W, Key_U, Key_M, Key_J, ___, ___,
   Key_Slash,  Key_V, Key_P, Key_Y, Key_Q, ___, ___,
   ___, OSM(LeftAlt), Key_Spacebar, LockLayer(NAVIGATION),
   ShiftToLayer(FUNCTION),
  ),
  [NAVIGATION] = KEYMAP_STACKED
  (___, ___, ___, ___, ___, ___, ___,
   ___, ___, ___, ___,___, ___, ___,
   ___, ___, M(NavToNumpad), M(NavToPunc), ___, ___,
   ___, ___, ___, ___, ___, ___, ___,
   ___, ___, ___, ___,
   ShiftToLayer(FUNCTION),

   ___,  ___, ___, ___,      ___,              ___, ___,
   ___,                    ___, ___, ___,      ___,              ___,      ___,
                           ___, ___, ___,      ___,              ___,         ___,
   ___,                    ___, ___, ___, ___, ___,   ___,
   ___, ___, ___, LockLayer(NAVIGATION),
   ShiftToLayer(FUNCTION)),
  [NUMPAD] = KEYMAP_STACKED
  (___, ___, ___, ___, ___, ___, ___,
   ___, ___, Key_1, Key_2,Key_3, Key_Backslash, ___,
   ___, ___, Key_4, Key_5, Key_6, Key_0,
   ___, ___, Key_7, Key_8, Key_9, ___, ___,
   ___, ___, ___, ___,
   ShiftToLayer(FUNCTION),

   ___,  ___, ___, ___,      ___,              ___, ___,
   ___,                    ___, ___, ___,      ___,              ___,      ___,
                           ___, ___, ___,      ___,              ___,         ___,
   ___,                    ___, ___, ___, ___, ___,   ___,
   ___, ___, ___, LockLayer(NUMPAD),
   ShiftToLayer(FUNCTION)),
  [PUNC] = KEYMAP_STACKED
  (___, ___, ___, ___, ___, ___, ___,
   ___, ___, ___, ___, ___, ___, ___,
   ___, ___, ___, ___, ___, ___,
   ___, ___, ___, ___, ___, ___, ___,
   ___, ___, ___, ___,
   ShiftToLayer(FUNCTION),

   ___, ___, ___, ___, ___, ___, ___,
   ___, ___, ___, ___, ___, ___, ___,
        ___, ___, ___, ___, ___, ___,
   ___, ___, ___, ___, ___, ___, ___,
   ___, ___, ___, LockLayer(PUNC),
   ShiftToLayer(FUNCTION)),
   [FUNCTION] =  KEYMAP_STACKED
   (___,      Key_F1,           Key_F2,      Key_F3,     Key_F4,        Key_F5,           ___,
     ___, ___, ___, Key_UpArrow, ___, ___, ___,
     ___, ___, Key_LeftArrow, Key_DownArrow, Key_RightArrow, ___,
     ___, ___, ___, ___, ___, ___, ___,
     ___, ___, ___, ___,
     ___,

    ___, Key_F6,                 Key_F7,                   Key_F8,                   Key_F9,          Key_F10,          Key_F11,
    ___, ___, ___, ___, ___, ___, ___,
         ___, ___, ___, ___, ___, ___,
    ___, ___, ___, ___, ___, ___, ___,
    ___, ___, ___, ___,
    ___)

)
/* *INDENT-ON* */

const macro_t *macroAction(uint8_t macroIndex, uint8_t keyState) {
    switch (macroIndex) {
        case NavToNumpad:
            return MACRODOWN(Tr(LockLayer(NAVIGATION)), Tr(LockLayer(NUMPAD)));
        case NavToPunc:
            return MACRODOWN(Tr(LockLayer(NAVIGATION)), Tr(LockLayer(PUNC)));
    }
    return MACRO_NONE;
};

void toggleLedsOnSuspendResume(kaleidoscope::plugin::HostPowerManagement::Event event) {
  switch (event) {
  case kaleidoscope::plugin::HostPowerManagement::Suspend:
    LEDControl.set_all_leds_to({0, 0, 0});
    LEDControl.syncLeds();
    LEDControl.paused = true;
    break;
  case kaleidoscope::plugin::HostPowerManagement::Resume:
    LEDControl.paused = false;
    LEDControl.refreshAll();
    break;
  case kaleidoscope::plugin::HostPowerManagement::Sleep:
    break;
  }
}

/** hostPowerManagementEventHandler dispatches power management events (suspend,
 * resume, and sleep) to other functions that perform action based on these
 * events.
 */
void hostPowerManagementEventHandler(kaleidoscope::plugin::HostPowerManagement::Event event) {
  toggleLedsOnSuspendResume(event);
}

/** This 'enum' is a list of all the magic combos used by the Model 01's
 * firmware The names aren't particularly important. What is important is that
 * each is unique.
 *
 * These are the names of your magic combos. They will be used by the
 * `USE_MAGIC_COMBOS` call below.
 */
enum {
  // Toggle between Boot (6-key rollover; for BIOSes and early boot) and NKRO
  // mode.
  COMBO_TOGGLE_NKRO_MODE,
  // Enter test mode
  COMBO_ENTER_TEST_MODE
};

/** Wrappers, to be used by MagicCombo. **/

/**
 * This simply toggles the keyboard protocol via USBQuirks, and wraps it within
 * a function with an unused argument, to match what MagicCombo expects.
 */
static void toggleKeyboardProtocol(uint8_t combo_index) {
  USBQuirks.toggleKeyboardProtocol();
}

/**
 *  This enters the hardware test mode
 */
static void enterHardwareTestMode(uint8_t combo_index) {
  HardwareTestMode.runTests();
}

/** Magic combo list, a list of key combo and action pairs the firmware should
 * recognise.
 */
USE_MAGIC_COMBOS({.action = toggleKeyboardProtocol,
                  // Left Fn + Esc + Shift
                  .keys = { R3C6, R2C6, R3C7 }
}, {
  .action = enterHardwareTestMode,
  // Left Fn + Prog + LED
  .keys = { R3C6, R0C0, R0C6 }
});

// First, tell Kaleidoscope which plugins you want to use.
// The order can be important. For example, LED effects are
// added in the order they're listed here.
KALEIDOSCOPE_INIT_PLUGINS(
  // The EEPROMSettings & EEPROMKeymap plugins make it possible to have an
  // editable keymap in EEPROM.
  EEPROMSettings,
  EEPROMKeymap,

  // Focus allows bi-directional communication with the host, and is the
  // interface through which the keymap in EEPROM can be edited.
  Focus,

  // FocusSettingsCommand adds a few Focus commands, intended to aid in
  // changing some settings of the keyboard, such as the default layer (via the
  // `settings.defaultLayer` command)
  FocusSettingsCommand,

  // FocusEEPROMCommand adds a set of Focus commands, which are very helpful in
  // both debugging, and in backing up one's EEPROM contents.
  FocusEEPROMCommand,

  // The boot greeting effect pulses the LED button for 10 seconds after the
  // keyboard is first connected
  BootGreetingEffect,

  // The hardware test mode, which can be invoked by tapping Prog, LED and the
  // left Fn button at the same time.
  HardwareTestMode,

  // LEDControl provides support for other LED modes
  LEDControl,

  // We start with the LED effect that turns off all the LEDs.
  LEDOff,

  // The HostPowerManagement plugin allows us to turn LEDs off when then host
  // goes to sleep, and resume them when it wakes up.
  HostPowerManagement,

  // The MagicCombo plugin lets you use key combinations to trigger custom
  // actions - a bit like Macros, but triggered by pressing multiple keys at the
  // same time.
  MagicCombo,

  // The USBQuirks plugin lets you do some things with USB that we aren't
  // comfortable - or able - to do automatically, but can be useful
  // nevertheless. Such as toggling the key report protocol between Boot (used
  // by BIOSes) and Report (NKRO).
  USBQuirks,

  // Mine
  OneShot,
  Macros
);

/** The 'setup' function is one of the two standard Arduino sketch functions.
 * It's called when your keyboard first powers up. This is where you set up
 * Kaleidoscope and any plugins.
 */
void setup() {
  // First, call Kaleidoscope's internal setup function
  Kaleidoscope.setup();

  // Set the action key the test mode should listen for to Left Fn
  HardwareTestMode.setActionKey(R3C6);

  // To make the keymap editable without flashing new firmware, we store
  // additional layers in EEPROM. For now, we reserve space for five layers. If
  // one wants to use these layers, just set the default layer to one in EEPROM,
  // by using the `settings.defaultLayer` Focus command, or by using the
  // `keymap.onlyCustom` command to use EEPROM layers only.
  EEPROMKeymap.setup(5);

}

/** loop is the second of the standard Arduino sketch functions.
  * As you might expect, it runs in a loop, never exiting.
  *
  * For Kaleidoscope-based keyboard firmware, you usually just want to
  * call Kaleidoscope.loop(); and not do anything custom here.
  */

void loop() {
  Kaleidoscope.loop();
}
