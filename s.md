one-shot shift/ctrl/alt/super
sticky-modifier key "<X>", so that <X><Shift><Ctrl><A> = shift-ctrl-a, or <X><Shift><Ctrl><X> = shift-ctrl, and maybe <X><X><Shift><A><B><X> = shift-a-b

shift, ctrl, alt, space, bksc, enter, + 2 "meta" keys in thumb row
tab, esc by index fingers
