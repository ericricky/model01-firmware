#!/bin/bash

git submodule update --init --recursive

wget https://raw.githubusercontent.com/keyboardio/Kaleidoscope/master/etc/99-kaleidoscope.rules
sudo cp 99-kaleidoscope.rules /etc/udev/rules.d
sudo /etc/init.d/udev reload

sudo usermod -a -G dialout $USER
newgrp dialout   # or su - $USER, or log out and in again
