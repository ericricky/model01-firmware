{ pkgs ? import <nixpkgs> {} }:
  pkgs.mkShell {
    buildInputs = [ pkgs.arduino ];
    ARDUINO_PATH = "${pkgs.arduino}/share/arduino";
}
